﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Services.Models;
using OpenQA.Selenium;

namespace AutomationDemo.Pages
{
	public class NewMailPage : BaseMailPage
	{
		private static readonly By sendMailButtonByCss = By.CssSelector("div.mail-Compose-Message button[type='submit']");
		private static readonly By inputToByCss = By.CssSelector("div[name='to']");
		private static readonly By inputSubjectdByCss = By.CssSelector("div.ns-view-compose-fields-box input[name='subj']");
		private static readonly By mailErrorMessageByCss = By.CssSelector("div.mail-Compose-Field-Notices-Items>div.ns-view-compose-field-to-error");
		private static readonly By messageDiscriptorsAreaByCss = By.CssSelector("div.ns-view-compose-fields-wrapper");

		public IWebElement SendMailButton { get { return browser.FindElement(sendMailButtonByCss); } }
		public IWebElement InputTo { get { return browser.FindElement(inputToByCss); } }
		public IWebElement InputSubject { get { return browser.FindElement(inputSubjectdByCss); } }
		public IWebElement MailErrorMessage { get { return browser.FindElement(mailErrorMessageByCss); } }
		public IWebElement MessageDiscriptorsArea { get { return browser.FindElement(messageDiscriptorsAreaByCss); } }

		public MailDonePage SendTestMessage(string userEmail)
		{
			InputTo.SendKeys(userEmail);
			InputSubject.SendKeys(MailSubject);
			SendMailButton.Click();
			return new MailDonePage();
		}

		public bool IsErrorMessage()
		{
			return MailErrorMessage.Displayed;
		}
	}
}
