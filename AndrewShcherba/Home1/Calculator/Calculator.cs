﻿namespace TestCalculator
{
    public class Program
    {
        static void Main()
        {

        }
    }

    public class Calculator
    {
        public int Sum(int x, int y) => x + y;
        public int Sub(int x, int y) => x - y;
        public int Mul(int x, int y) => x * y;
        public int Div(int x, int y) => x / y;
    }
}
