﻿using System;
using System.Linq;
using NUnit.Framework;

namespace ASP_TASK1.Tests
{
    [TestFixture]
    class SimpleTests
    {
        Something somethingNull;
        Something something;

        [SetUp]
        public void SetUp()
        {
            somethingNull = new Something();
            something = new Something(6, 2, 3);
        }

        [Test] //it's be failed
        public void Test_to_return()
        {
            int expected = 4;
            var actual = something.ReturnSomething();
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Test_to_String()
        {
            string expected = "Something:6-2-3";
            var actual = something.ToString();
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Test_null_should_be_excepted()
        {            
            Assert.Throws<DivideByZeroException>(() => somethingNull.ReturnSomething());
        }

        [TestCase(3, 3, 3, 3)]
        [TestCase(3, 3, 9, 1)]
        [TestCase(10, 10, 2, 50)]
        public void MultipleTest(int a, int b, int c, int expected)
        {
            Something some = new Something(a, b, c);
            var actual = some.ReturnSomething();
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(expected, a * b / c);
        }
        
        [TearDown]
        public void TearDown()
        {
            somethingNull = null;
            something = null;
        }
    }
}
