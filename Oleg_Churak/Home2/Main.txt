﻿На странице google.com :
1. Строка для ввода кейворда для поиска
XPATH: .//input[@id='lst-ib']
CSS: #lst-ib
2. Кнопка "Поиск в Google"
XPATH: .//input[@name='btnK']
CSS: input[name=btnK]
3. Кнопка "Мне повезет"
XPATH: .//input[@name='btnI']
CSS: input[name=btnI]

На странице результатов гугла:
1. Ссылки на результаты поиска (все)
XPATH: .//div[@id='rso']
CSS: #rso
2. 5-я буква о в Goooooooooogle (внизу, где пагинация)
XPATH: .//table[@id='nav']//td[6]//span
CSS: #nav td:nth-child(6) span

На странице yandex.com:
1. Login input field
XPATH: .//input[@name='login']
CSS: input[name=login]
2. Password input field
XPATH: .//input[@name='passwd']
CSS: input[name=passwd]
3. "Enter" button in login form
XPATH: .//form[contains(@class, 'domik2')]//button
CSS: .button.auth__button

На странице яндекс почты :
1. Ссылка "Входящие"
XPATH: .//div[contains(@class,'ns-view-folders')]//a[@href='#inbox']
CSS:  a.ns-view-folder[href='#inbox']
2. Ссылка "Исходящие"
XPATH: .//div[contains(@class,'ns-view-folders')]//a[@href='#sent']
CSS: a.ns-view-folder[href='#sent']
3. Ссылка "Спам"
XPATH: .//div[contains(@class,'ns-view-folders')]//a[@href='#spam']
CSS: a.ns-view-folder[href='#spam']
4. Ссылка "Удаленные"
XPATH: .//div[contains(@class,'ns-view-folders')]//a[@href='#trash']
CSS: a.ns-view-folder[href='#trash']
5. Ссылка "Черновики"
XPATH: .//div[contains(@class,'ns-view-folders')]//a[@href='#draft']
CSS: a.ns-view-folder[href='#draft']
6. Кнопка "Новое письмо"
XPATH: .//a[@href='#compose']
CSS: a[href='#compose']
7. Кнопка "Обновить"
XPATH: .//div[@data-click-action='mailbox.check']
CSS: div[data-click-action='mailbox.check']
8. Кнопка "Отправить" (на странице нового письма)
XPATH:  //div[contains(@class, 'mail-Compose-Message')]//button
CSS: div.mail-Compose-Message button
9. Кнопка "Пометить как спам"
XPATH: .//div[contains(@class,'button-spam')]
CSS: div.ns-view-toolbar-button-spam
10. Кнопка "Пометить прочитанным"
XPATH: .//div[contains(@class,'mark-as-read')]
CSS: div.ns-view-toolbar-button-mark-as-read
11. Кнопка "Переместить в другую директорию"
XPATH: .//div[contains(@class,'button-folders-actions')]
CSS: div.ns-view-toolbar-button-folders-actions
12. Кнопка "Закрепить письмо"
XPATH: .//div[contains(@class,'button-pin')]
CSS: div.ns-view-toolbar-button-pin
13. Селектор для поиска уникального письма
XPATH: .//input[contains(@class,'search-input')]
CSS: span._nb-input-content > input.js-search-input

На странице яндекс диска :
1. Кнопка загрузить файлы
XPATH: .//input[@class="button__attach"]
CSS: input.button__attach
2. Селектор для уникального файла на диске
XPATH: .//input[@class="input__control"]
CSS: input.input__control
3. Кнопка скачать файл
XPATH: .//button[@data-click-action='resource.download']
CSS: button[data-click-action='resource.download']
4. Кнопка удалить файл
XPATH: .//button[@data-click-action='resource.delete']
CSS: button[data-click-action='resource.delete']
5. Кнопка в корзину
XPATH: .//button[@data-click-action='resource.delete']
CSS: button[data-click-action='resource.delete']
6. Кнопка восстановить файл
XPATH: .//button[@data-click-action='resource.restore']
CSS: button[data-click-action='resource.restore']