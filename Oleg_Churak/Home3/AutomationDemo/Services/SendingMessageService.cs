﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Models;
using AutomationDemo.Pages;
using System.Threading;

namespace AutomationDemo.Services
{
    public class SendingMessageService
    {
        private SendingMailPage sendingMailPage = new SendingMailPage();
        private ListOfLettersPage listOfLettersPage = new ListOfLettersPage();
        private MainMailStaticPage mainMailStaticPage = new MainMailStaticPage();

        public void SendMail(MailMessage mailMessage)
        {
            mainMailStaticPage.SendNewMessageLink.Click();

            sendingMailPage.SendToRecipientInput.SendKeys(mailMessage.Recipient);
            sendingMailPage.SendToSubjectInput.SendKeys(mailMessage.Subject);
            sendingMailPage.ContentOfLetterTextArea.SendKeys(mailMessage.TextMessage);
            sendingMailPage.SendingMailButton.Click();

            mainMailStaticPage.InboxLink.Click();
        }

        public bool IsFoundSentLetter(MailMessage mailMessage)
        {
            mainMailStaticPage.RefreshMailBoxLink.Click();
            bool isResultOnInboxPage = listOfLettersPage.LetterLink(mailMessage.Subject) != null;

            mainMailStaticPage.SentMailLink.Click();
            bool isResultOnSentMailPage = listOfLettersPage.LetterLink(mailMessage.Subject) != null;

            return (isResultOnInboxPage && isResultOnSentMailPage);
        }
    }
}
