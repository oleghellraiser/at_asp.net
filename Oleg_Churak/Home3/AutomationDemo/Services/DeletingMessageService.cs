﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Models;
using AutomationDemo.Pages;

namespace AutomationDemo.Services
{
    public class DeletingMessageService
    {

        private SendingMailPage sendingMailPage = new SendingMailPage();
        private ListOfLettersPage listOfLettersPage = new ListOfLettersPage();
        private MainMailStaticPage mainMailStaticPage = new MainMailStaticPage();

        public void DeleteMessege(MailMessage mailMessage)
        {
            mainMailStaticPage.TrashLink.Click();
            mainMailStaticPage.RefreshMailBoxLink.Click();
            listOfLettersPage.LetterCheckbox(mailMessage.Subject).Click();
            mainMailStaticPage.MoveMessageToTrashLink.Click();

            //Double deleting if used mailbox is the only one 
            listOfLettersPage.LetterCheckbox(mailMessage.Subject).Click();
            mainMailStaticPage.MoveMessageToTrashLink.Click();
        }

        public bool IsNotFoundDeletedLetter(MailMessage mailMessage)
        {
            mainMailStaticPage.TrashLink.Click();
            mainMailStaticPage.RefreshMailBoxLink.Click();
            return (listOfLettersPage.LetterLink(mailMessage.Subject) == null);
        }
    }
}
