﻿using AutomationDemo.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationDemo.Pages
{
    class ListOfLettersPage
    {
        private Browser browser = Browser.Instance;

        //private static readonly By letterCheckboxByCss = By.CssSelector("input._nb-checkbox-input[type='checkbox']");
        private static readonly By letterCheckboxByCss = By.CssSelector("label[data-nb='checkbox']");
        private static By letterLinkByXPath(string necesserySubject)
            => By.XPath($".//span[@title='{necesserySubject}']/ancestor::a");

        internal IWebElement LetterLink(string necesserySubject)
        {
                browser.TimeOut(5);
                return browser.FindElement(letterLinkByXPath(necesserySubject), 10);
        }
        internal IWebElement LetterCheckbox(string necesserySubject) =>
            LetterLink(necesserySubject).FindElement(letterCheckboxByCss);
    }
}
