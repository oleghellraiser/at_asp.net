﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Threading;

namespace AutomationDemo.Framework
{
    public class Browser
    {
        private const string chromeDriverPath = "D:/";
        private static readonly TimeSpan implicitlyWait = TimeSpan.FromSeconds(15);
        private static readonly TimeSpan pageLoadWait = TimeSpan.FromSeconds(15);

        private IWebDriver driver;

        private static Lazy<Browser> instanceHolder = new Lazy<Browser>(() => new Browser());

        public static Browser Instance
        {
            get { return instanceHolder.Value; }
        }

        private Browser() { }

        public Browser Start()
        {
            // It isn't good practice to init and setup driver here this way.
            // Better use driver factories, config files etc. But for our purposes we can leave it for now.
            driver = new ChromeDriver(chromeDriverPath);
            driver.Manage().Timeouts().ImplicitlyWait(implicitlyWait);
            driver.Manage().Timeouts().SetPageLoadTimeout(pageLoadWait);
            driver.Manage().Window.Maximize();
            return this;
        }

        public void Close()
        {
            if (driver != null) driver.Close();
            driver = null;
        }

        public void OpenAt(string url) => driver.Url = url;

        public void TimeOut(int timeoutInSeconds)
        {
            if (timeoutInSeconds > 0)
            {
                Thread.Sleep(timeoutInSeconds * 1000);
            }
        }

        internal IWebElement FindElement(By by)
        {
            try
            {
                return driver.FindElement(by);
            }
            catch (NoSuchElementException)
            {
                return null;
            }           
        }

        internal IWebElement FindElement(By by, int timeoutInSeconds)
        {
            try
            {
                if (timeoutInSeconds > 0)
                {
                    var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));
                    return wait.Until(drv => drv.FindElement(by));
                }
                return driver.FindElement(by);
            }
            catch (WebDriverTimeoutException)
            {
                return null;
            }
            catch (NoSuchElementException)
            {
                return null;
            }
        }
    }
}
