﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace webdriver_sandbox
{
	class Program
	{
		static void Main(string[] args)
		{
			//CheckoutSearchPage();
			//ManageCookies();
			//WaitExplicitly();
		}

		static void CheckoutSearchPage()
		{
			using (IWebDriver driver = new ChromeDriver())
			{
				//driver.Url = "http://google.com";
				//var searchInputCssSelector = "input#lst-ib";
				//var searchInput = driver.FindElement(By.CssSelector(searchInputCssSelector));

				//var searchBtnCssSelector = "input[name='btnK']";
				//var searchBtn = driver.FindElement(By.CssSelector(searchBtnCssSelector));

				//searchInput.SendKeys("selenium webdriver example");
				//searchBtn.Click();

				driver.Navigate().GoToUrl("http://www.google.com");
				IWebElement query = driver.FindElement(By.Name("q"));

				query.SendKeys("selenium webdriver");
				query.Submit();

				var handles = driver.WindowHandles;

				var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
				wait.Until(d => d.Title.StartsWith("selenium", StringComparison.OrdinalIgnoreCase));

				Console.WriteLine("Page: " + driver.Title);

			}
		}

		static void ManageCookies()
		{
			using (IWebDriver driver = new ChromeDriver())
			{
				driver.Url = "http://google.com";
				Cookie cookie = new Cookie("SID", 
					"4wPrHtt0lV3SuW6imnfTvUBnlhZUz8nv75IqZ6w-wg6hEYXJU_6Cbbg9ndiAPyLV94KGQQ.", 
					".google.by", 
					"/", 
					new DateTime(2018,10,17));
				driver.Manage().Cookies.AddCookie(cookie);
				var allCookies = driver.Manage().Cookies;
				foreach (var item in allCookies.AllCookies)
				{
					Console.WriteLine(item.Name + " " + item.Value);
				}

				driver.Manage().Cookies.DeleteCookieNamed("SID");
			}
		}

		static void WaitExplicitly()
		{
			using (var driver = new ChromeDriver())
			{
				driver.Url = "http://gmail.com";
				var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(2));
				try
				{
					var details = wait.Until<IWebElement>(d => d.FindElement(By.Id(":2q")));
				}
				catch (WebDriverTimeoutException ex)
				{
					Console.WriteLine("Could not open gmail");
					//throw ex;
				}
			}
		}
	}
}
